import React, { Component } from 'react';
// import logo from './logo.svg';

import SignInForm from './pages/SignInForm'
import SignUpForm from './pages/SignUpForm'
import Admin from './pages/Admin'
import Logout from './pages/Logout'
import { HashRouter as Router, Route,  } from 'react-router-dom';


export default class HelloWorld extends Component {
  constructor(){
    super();
    this.state={
      error: null,
      isLoaded: false,
      items: []
    }
  }

  render() {
    return (
      <Router>
        <div>
        <Route exact path="/" component={SignInForm} />
        <Route path="/signup" component={SignUpForm} />
        <Route path="/admin" component={Admin} />
        <Route path="/logout" component={Logout} />
        </div>
      </Router>
    );
  }
}

