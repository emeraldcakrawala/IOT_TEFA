import React, { Component } from 'react';
import { HashRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import SignUpForm from './pages/SignUpForm';
import SignInForm from './pages/SignInForm';
import Admin from './pages/Admin';
import Logout from './pages/Logout';

import './App.css';

class App extends Component {
  render() {
    return (
      <Router basename="/react-auth-ui/">
        <div className="App">
          {/* <div className="App__Aside"></div> */}
          <div className="App__Form">
            <div className="PageSwitcher">
                <NavLink exact to="/" activeClassName="PageSwitcher__Item--Active" className="PageSwitcher__Item">Sign In</NavLink>
                <NavLink  to="/sign-up" activeClassName="PageSwitcher__Item--Active" className="PageSwitcher__Item">Sign Up</NavLink>
              </div>

              <div className="FormTitle">
                  
                  {/* <NavLink 
                  to="/sign-in" 
                  activeClassName="FormTitle__Link--Active" 
                  className="FormTitle__Link">Sign In</NavLink> */}
                   {/* or 
                   <NavLink 
                   exact to="/" 
                   activeClassName="FormTitle__Link--Active" 
                   className="FormTitle__Link">Sign Up</NavLink> */}
              </div>

              
              {/* <Route exact path="/" component={SignUpForm}>
              </Route> */}
              {/* <Route path="/sign-in" component={SignInForm}>
              </Route> */}
              <Route exact path="/" component={SignInForm}>
              </Route>
              <Route path="/sign-up" component={SignUpForm}>
              </Route>
              {/* <Route path="/admin" component={Admin}>
              </Route>
              <Route path="/logout" component={Logout}>
              </Route> */}

              
          </div>

        </div>
      </Router>
    );
  }
}

export default App;
