import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import './Logout.css'

export default class Logout extends Component {
  constructor(props){
    super(props)
    localStorage.removeItem("token")
  }
  render() {
    return (
      <div className="Logout">
        <h1>Anda yakin ingin Logout ?</h1>
        <div className="link">
        <Link to="/" style={{color: 'white', textDecoration:'none'}}>Logout</Link>
        </div>
      </div>
    )
  }
}
