import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './SignInForm.css';

class SignUpForm extends Component {
    constructor() {
        super();

        this.state = {
            email: '',
            password: '',
            name: '',
            hasAgreed: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
          [name]: value
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        console.log('The form was submitted with the following data:');
        console.log(this.state);
    }

    

    render() {
        return (
            <div className="Forms">
        <div className="FormsCenter">
        <div className="Down">
        <h1> SIGN UP </h1>
            <form onSubmit={this.handleSubmit} className="FormssFields" style={{paddingTop: "50px"}}>
              <div className="FormsField">
                <label className="FormField__Label" htmlFor="name">Full Name</label> <br/>
                <input type="text" id="name" className="FormsField__Input" placeholder="Enter your full name" name="name" value={this.state.name} onChange={this.handleChange} />
              </div>
              <div className="FormsField">
                <label className="FormField__Label" htmlFor="password">Password</label> <br/>
                <input type="password" id="password" className="FormsField__Input" placeholder="Enter your password" name="password" value={this.state.password} onChange={this.handleChange} />
              </div>
              <div className="FormsField">
                <label className="FormField__Label" htmlFor="email">E-Mail Address</label> <br/>
                <input type="email" id="email" className="FormsField__Input" placeholder="Enter your email" name="email" value={this.state.email} onChange={this.handleChange} />
              </div>

              {/* <div className="FormField">
                <label className="FormField__CheckboxLabel">
                    <input className="FormField__Checkbox" type="checkbox" name="hasAgreed" value={this.state.hasAgreed} onChange={this.handleChange} /> I agree all statements in terms of service
                </label>
              </div> */}

              <div className="FormsField">
                  <button className="FormsField__Button mr-20">Sign Up</button> <br/> <br/>
                  <Link to="/" className="FormField__Link">I'm already member</Link>
              </div>
            </form>
          </div>
          </div>
          </div>
        );
    }
}

export default SignUpForm;
