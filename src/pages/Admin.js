import React, { Component } from 'react'
import { Link, Redirect, Route, Switch } from 'react-router-dom'
import AdminNavbar from "./Navbar/AdminNavbar";
import add from './../add.png';
import './Admin.css'
import Card from './Card/Card'
import axios from 'axios';
import Typography from '@material-ui/core/Typography';
import { Button } from '@material-ui/core';
import bukapintu from './../bukapintu.png';


export default class Admin extends Component {

  state = {
    // auth: true,
    expanded: null,
    // devive_name: "",
  };


  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  constructor(props) {
    super(props)
    this.state = {isToggleOn: true};
    const token = localStorage.getItem("token")
    let loggedIn = true
    if (token == null) {
      loggedIn = false
    }
    this.state = {
      loggedIn,
      value: 0,
      checked: false,
      liked: false,
      items: [],
      data: [],
      status: ''

    }
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
  // componentDidMount = async () => {
  componentDidMount() {
    // const a = await axios
    axios.get(`http://180.250.162.213:6996/device`)
      .then(response => {
        console.log(response.data)
        this.setState({ data: response.data })
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  handleChangeRange = event => {
    this.setState({
      value: event.target.valueAsNumber,
    });
  };

  handleClick(data) {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
    console.log(data.status)
    if(data.status === 0){
      axios.put(`http://180.250.162.213:6996/device/on/`+data._id)
      .then(response => {
        console.log(response.data)
        this.setState({status: response.data.status})
      })
      .catch(error => {
        console.log(error);
      });
    }else{
      axios.put(`http://180.250.162.213:6996/device/off/`+data._id)
      .then(response => {
        console.log(response.data)
        this.setState({status: response.data.status})
      })
      .catch(error => {
        console.log(error);
      });
    }
      
  }

  render() {
    let styles = {
      color: 'white',
      //margin: '10px',
      padding: '10px',
      cursor: 'pointer',
      background: '#8b1616',
      borderRadius: '5px',
      width: '90px',
      marginLeft: '2%',
    };

    // const text = this.state.liked ? 'ON' : 'OFF';

    const { classes } = this.props;
    const { expanded, data } = this.state;

    if (this.state.loggedIn === false) {
      return <Redirect to="/" />
    }

    return (
      <div className="root" >
        <AdminNavbar />
        <div ><img className="bukapintu" src={bukapintu}/></div>
        <div className="tombol">
        {data.map(function (data, index) {
          return (
            <div className="card" key={index} >
              <div class="container">
                <h1>
                  {data.device_name}
                  <Typography>{data.type}</Typography>
                </h1>
                <Button onClick={() => this.handleClick(data)} style={styles}>
                {data.status}
                </Button>
              </div>
            </div>
          );
        }.bind(this))}
        </div>
       

         

        {/* <div class="card2">
          <div class="container">
            <button>
              <img src={add} style={{ width: '170px', paddingTop: '10px' }} />
            </button>
          </div>
        </div> */}


      </div>



    )
  }
}