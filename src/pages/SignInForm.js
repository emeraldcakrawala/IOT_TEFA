import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import './SignInForm.css';
import user from './../user.png';
import lock from './../lock.png';
// import SignInStyle from './SignInStyle';

class SignInForm extends Component {
    constructor(props) {
        super(props);
        const token = localStorage.getItem("token")

        let loggedIn = true
        if(token == null){
          loggedIn = false
        }

        this.state = {
            email: '',
            password: '',
            loggedIn
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
          [name]: value,
          [e.target.name]: e.target.value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const { email, password } = this.state
        //login magic
        if(email === "aku@aku.com" && password === "aku"){
          localStorage.setItem("token", "lalalalala")
          this.setState({
            loggedIn: true
          })
        }

        console.log('The form was submitted with the following data:');
        console.log(this.state);
    }

    render() {
      if(this.state.loggedIn){
        return <Redirect to="/admin"/>
      }
        return (
          <div className="Form">
        <div className="FormCenter">
        <div className="Up">
        <h1> SIGN IN </h1>
          </div>
            <form onSubmit={this.handleSubmit} className="FormFields">
            <div className="FormField">
            <img src={user} style={{width: '19px', position: 'absolute', marginLeft: '17px', marginTop: '10px'}}/>
                <input type="email" id="email" className="FormField__Input" placeholder="email" name="email" value={this.state.email} onChange={this.handleChange} />
              </div>

              <div className="FormField">
              <img src={lock} style={{width: '19px', position: 'absolute', marginLeft: '17px', marginTop: '10px'}}/>
                <input type="password" id="password" className="FormField__Input" placeholder="Enter your password" name="password" value={this.state.password} onChange={this.handleChange} />
              </div>

              <div className="FormField">
                  <button className="FormField__Button">Sign In</button> <br/> <br/>
                  <Link to="/signup" className="FormField__Link">Create an account</Link>
              </div>
            </form>
          </div>
          </div>
        );
    }
}

export default SignInForm;
